const express = require("express");
const bodyParser = require("body-parser");
const { getAllProduct, getProductByCode } = require("./product-service");

const app = express();
app.use(bodyParser.json());

app.get("/product", (req, res) => {
  res.json(getAllProduct());
});

app.get("/product/:code", (req, res) => {
  const product = getProductByCode(req.params.code);
  if (!product) return res.status(404).send("Product not found");
  res.json(product);
});

//Need to calculate total and discount
app.post("/checkout", (req, res) => {

  const productos = getAllProduct();
  let totalAPagar = 0;
  let totalDescuento = 0;
  productos.forEach(producto => { 
        totalAPagar += producto.price * ((100-producto.discount)/100) ;
        totalDescuento += producto.discount;
      }
  );

  const result = {
    total: productos.length,
    totalDiscount: totalDescuento,
    totalToPay: totalAPagar,
    product: productos
  };
  res.json(result);
  
});

app.listen(3000);
console.log("Express started on port 3000");
